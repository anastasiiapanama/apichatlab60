import React, {useState, useEffect} from 'react';
import './Chat.css';
import Post from "../../components/Post/Post";
import MessageForm from "../../components/MessageForm/MessageForm";
import axios from 'axios';
import moment from 'moment';
import {
    Avatar, Divider,
    Grid,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    makeStyles,
    Paper, TextField,
    Typography
} from "@material-ui/core";

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
    chatSection: {
        width: '100%',
        height: '80vh'
    },
    headBG: {
        backgroundColor: '#e0e0e0'
    },
    borderRight500: {
        borderRight: '1px solid #e0e0e0'
    },
    messageArea: {
        height: '70vh',
        overflowY: 'auto'
    }
});

const Chat = () => {
    const classes = useStyles();

    const [messages, setMessages] = useState([]);
    const [newMessage, setNewMessage] = useState('');
    const [lastDate, setLastDate] = useState('');
    let interval = null;

    const url = 'http://146.185.154.90:8000/messages';

    const getFirstMessages = async () => {
        try {
            const response = await axios.get(url);
            setMessages(response.data);
            setLastDate(response.data[response.data.length - 1].datetime);
        } catch (e) {
            console.log(e);
        }
    }

    useEffect(() => {
        getFirstMessages()
    }, [])

    useEffect(() => {
        if(lastDate) {
            interval = setInterval(async () => {
                const lastUrl = url + `?datetime=${lastDate}`
                const response = await fetch(lastUrl);

                if (response.ok) {
                    const lastMessages = await response.json();

                    const messagesCopy = [...messages];
                    messagesCopy.concat(lastMessages);
                    setMessages(messagesCopy);

                    if(lastMessages.length > 0) {
                        setLastDate(lastMessages[lastMessages.length - 1].datetime)
                    }
                }
            }, 2000);
        }
        return () => clearInterval(interval)
    }, [lastDate]);

    const onChangeMessage = e => {
        const messageValue = e.target.value;

        setNewMessage(messageValue);
    }

    const sendMessage = async () => {
        try {
            const data = new URLSearchParams();
            data.set('message', newMessage);
            data.set('author', 'Anastasia');
            await axios.post(url, data);
        } catch (e) {
            console.log(e);
        }

        setNewMessage("");
    }

    return (
        <div>
            <Grid container>
                <Grid item xs={12} >
                    <Typography variant="h5" className="header-message">Chat</Typography>
                </Grid>
            </Grid>
            <Grid container component={Paper} className={classes.chatSection}>
                <Grid item xs={3} className={classes.borderRight500}>
                    <List>
                        <ListItem button key="Anastasiia">
                            <ListItemIcon>
                                <Avatar alt="Anastasiia" src="https://material-ui.com/static/images/avatar/3.jpg" />
                            </ListItemIcon>
                            <ListItemText primary="Anastasiia"></ListItemText>
                        </ListItem>
                    </List>
                    <Divider />
                    <Grid item xs={12} style={{padding: '10px'}}>
                        <TextField id="outlined-basic-email" label="Search" variant="outlined" fullWidth />
                    </Grid>
                    <Divider />
                    <List>
                        <ListItem button key="RemySharp">
                            <ListItemIcon>
                                <Avatar alt="Remy Sharp" src="https://material-ui.com/static/images/avatar/1.jpg" />
                            </ListItemIcon>
                            <ListItemText primary="Remy Sharp">Remy Sharp</ListItemText>
                            <ListItemText secondary="online" align="right"></ListItemText>
                        </ListItem>
                        <ListItem button key="Alice">
                            <ListItemIcon>
                                <Avatar alt="Alice" src="https://material-ui.com/static/images/avatar/3.jpg" />
                            </ListItemIcon>
                            <ListItemText primary="Alice">Alice</ListItemText>
                        </ListItem>
                        <ListItem button key="CindyBaker">
                            <ListItemIcon>
                                <Avatar alt="Cindy Baker" src="https://material-ui.com/static/images/avatar/2.jpg" />
                            </ListItemIcon>
                            <ListItemText primary="Cindy Baker">Cindy Baker</ListItemText>
                        </ListItem>
                    </List>
                </Grid>
                <Grid item xs={9}>
                    <List className={classes.messageArea}>
                        {messages.map(mes => (
                            <Post
                                key={mes.id}
                                message={mes.message}
                                author={mes.author}
                                date={moment(mes.datetime).format('HH:mm:ss')}
                            />
                        ))}
                    </List>
                    <Divider />
                    <MessageForm
                        changeMessage={e => onChangeMessage(e)}
                        message={newMessage}
                        sendHandler={sendMessage}
                    />
                </Grid>
            </Grid>
        </div>
    );
};

export default Chat;