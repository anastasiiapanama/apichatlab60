import React from 'react';
import {Fab, Grid, TextField} from "@material-ui/core";
import SendIcon from '@material-ui/icons/Send';

const MessageForm = props => {
    return (
        <Grid container style={{padding: '20px'}}>
            <Grid item xs={11}>
                <TextField id="outlined-basic-email" label="Type Something" value={props.message} onChange={props.changeMessage} fullWidth />
            </Grid>
            <Grid xs={1} align="right">
                <Fab color="primary" aria-label="add" onClick={props.sendHandler}><SendIcon /></Fab>
            </Grid>
        </Grid>
    );
};

export default MessageForm;