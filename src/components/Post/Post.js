import React from 'react';
import './Post.css';
import {Grid, ListItem, ListItemText} from "@material-ui/core";

const Post = props => {
    return (
        <ListItem key="2">
            <Grid container>
                <Grid item xs={12}>
                    <ListItemText align="left" primary={props.message}></ListItemText>
                </Grid>
                <Grid item xs={12}>
                    <ListItemText align="left" secondary={props.author}></ListItemText>
                    <ListItemText align="left" secondary={props.date}></ListItemText>
                </Grid>
            </Grid>
        </ListItem>
    );
};

export default Post;